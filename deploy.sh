#!/bin/bash

source /etc/rsiprogress.conf

NET=rsi_net

docker network create $NET

if [[ $* == *--proxy* ]]; then
    docker pull nginx
    docker run -p 80:80 --name=rproxy --restart=unless-stopped --net=$NET -d nginx
    sleep 2s
    docker cp proxy.conf rproxy:/etc/nginx/conf.d/default.conf
    docker restart rproxy
fi

if [[ $* == *--db* ]]; then
    docker pull mongo
    docker run --name=mongo --restart=unless-stopped --net=$NET -d mongo --auth
    sleep 2s
    docker exec -it mongo mongo admin --eval 'db.createUser({user:"'$ADMIN_USR'",pwd:"'$ADMIN_PWD'",roles:[{role:"userAdminAnyDatabase",db:"admin"}]})'
    docker exec -it mongo mongo admin -u $ADMIN_USR -p $ADMIN_PWD --eval 'db.createUser({user:"'$USR'",pwd:"'$PWD'",roles:[{role:"readWrite",db:"rsiprogress"}]})'
fi

if [[ $* == *--backend* ]]; then
    docker stop backend
    docker rm backend
    docker rmi rsi-backend
    docker build -t rsi-backend src/backend
    docker run --name=backend --restart=unless-stopped --net=$NET -e "MONGO_USR="$USR -e "MONGO_PWD="$PWD -e "PRODUCTION_MODE=1" -d rsi-backend
fi

if [[ $* == *--frontend* ]]; then
    docker stop frontend
    docker rm frontend
    docker rmi rsi-frontend
    docker build -t rsi-frontend src/frontend
    docker run --name=frontend --restart=unless-stopped --net=$NET -d rsi-frontend
fi
