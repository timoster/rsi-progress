package main

import (
	"net/http"
	"os"

	"backend/endpoints"
	"backend/global"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2"
)

func ensureIndex(s *mgo.Session, collection string, unique bool, key ...string) {
	session := s.Copy()
	defer session.Close()

	c := session.DB(global.DBName).C(collection)
	index := mgo.Index{
		Key:        key,
		Unique:     unique,
		DropDups:   unique,
		Background: true,
		Sparse:     true,
	}

	if err := c.EnsureIndex(index); err != nil {
		panic(err)
	}
}

func main() {
	session, err := mgo.Dial("mongo:27017")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	if os.Getenv("PRODUCTION_MODE") == "1" {
		gin.SetMode(gin.ReleaseMode)
		session.Login(&mgo.Credential{Username: os.Getenv("MONGO_USR"), Password: os.Getenv("MONGO_PWD")})
	}

	ensureIndex(session, global.CollectionUsers, true, "email", "api_key")
	ensureIndex(session, global.CollectionReports, false, "month", "day", "user")

	r := gin.Default()
	r.Use(func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With, X-Api-Key")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	})

	r.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "rsiProgress API v0.0.1")
	})

	r.POST("/auth/login", endpoints.Login(session))
	r.POST("/auth/register", endpoints.Register(session))

	auth := r.Group("/auth")
	auth.Use(endpoints.AuthMiddleware(session))
	{
		auth.GET("/logout", endpoints.Logout(session))
		auth.GET("/check", endpoints.CheckAuth(session))
		auth.DELETE("/delete", endpoints.Delete(session))
		auth.POST("/update", endpoints.Update(session))
	}

	user := r.Group("/user")
	user.Use(endpoints.AuthMiddleware(session))
	{
		user.GET("/categories", endpoints.GetCategories(session))
		user.POST("/categories", endpoints.AddCategory(session))
		user.GET("/registration", endpoints.GetRegistrationMonth(session))
		user.GET("/reports/:year/:month", endpoints.GetReports(session))
		user.POST("/reports", endpoints.AddReport(session))
		user.GET("/allowed", endpoints.AllowedToReport(session))
	}

	r.Run()
}
