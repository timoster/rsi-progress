package global

// DBName is the database name for MongoDB
const DBName string = "rsiprogress"

// CollectionUsers is the collection name for users for MongoDB
const CollectionUsers string = "users"

// CollectionReports is the collection name for reports for MongoDB
const CollectionReports string = "reports"

// APIHeaderKey is used to get the api-key from the request header
const APIHeaderKey string = "X-Api-Key"
