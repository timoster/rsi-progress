package structs

import (
	"gopkg.in/mgo.v2/bson"
)

// Month holds data to store months
type Month struct {
	Year  int `json:"year" bson:"year"`
	Month int `json:"month" bson:"month"`
}

// Category holds the report data for each category
type Category struct {
	Name  string `json:"name" bson:"name"`
	Time  int    `json:"time" bson:"time"`
	Skill int    `json:"skill" bson:"skill"`
	Pain  int    `json:"pain" bson:"pain"`
}

// Report holds the data for the report of a day
type Report struct {
	User       bson.ObjectId `json:"-" bson:"user"`
	Month      Month         `json:"month" bson:"month"`
	Day        int           `json:"day" bson:"day"`
	Categories []Category    `json:"categories" bson:"categories"`
}
