package structs

import (
	"gopkg.in/mgo.v2/bson"
)

// User represents the user in the users collection in MongoDB
type User struct {
	ID       bson.ObjectId `bson:"_id,omitempty"`
	Name     string        `json:"name" bson:"name"`
	Email    string        `json:"email" bson:"email"`
	Password string        `json:"password" bson:"password"`
	APIKey   string        `json:"api_key" bson:"api_key"`

	RegistrationMonth Month    `json:"registration_month" bson:"registration_month"`
	Categories        []string `json:"categories" bson:"categories"`
}

// UserUpdate is the json layout for update calls
type UserUpdate struct {
	Name        string `json:"name" bson:"name"`
	Email       string `json:"email" bson:"email"`
	Password    string `json:"password" bson:"password"`
	NewPassword string `json:"new_password" bson:"new_password"`
}
