package endpoints

import (
	"crypto"
	"encoding/base64"
	"time"

	"backend/global"
	"backend/structs"

	"encoding/json"
	"net/http"
	"regexp"

	"github.com/gin-gonic/gin"
	"github.com/satori/go.uuid"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var nameRegex *regexp.Regexp
var emailRegex *regexp.Regexp

func init() {
	nameRegex, _ = regexp.Compile("^[\\d\\sa-zA-Z]{3,}$")
	emailRegex, _ = regexp.Compile("^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$")
}

func sha256(input string) string {
	hash := crypto.SHA256.New()
	hash.Write([]byte(input))
	return base64.URLEncoding.EncodeToString(hash.Sum(nil))
}

// AuthMiddleware is the middleware for all authorized server calls
func AuthMiddleware(s *mgo.Session) gin.HandlerFunc {
	return func(c *gin.Context) {
		session := s.Copy()
		defer session.Close()

		apiKey := c.Request.Header[global.APIHeaderKey]

		if apiKey == nil || len(apiKey) < 1 {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "Invalid API-Key"})
			return
		}

		collection := session.DB(global.DBName).C(global.CollectionUsers)
		count, err := collection.Find(bson.M{"api_key": apiKey[0]}).Count()

		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
			return
		}

		if count == 0 {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "You are not logged in"})
			return
		}

		c.Set("api-key", apiKey[0])
		c.Next()
	}
}

// Login returns the gin handling function for login requests
func Login(s *mgo.Session) gin.HandlerFunc {
	return func(c *gin.Context) {
		session := s.Copy()
		defer session.Close()

		var user structs.User
		decoder := json.NewDecoder(c.Request.Body)

		if decoder.Decode(&user) != nil || !emailRegex.MatchString(user.Email) {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid body"})
			return
		}

		user.Password = sha256(user.Password)

		collection := session.DB(global.DBName).C(global.CollectionUsers)
		if collection.Find(bson.M{"email": user.Email, "password": user.Password}).One(&user) != nil {
			c.JSON(http.StatusForbidden, gin.H{"error": "Invalid credentials"})
			return
		}

		user.APIKey = uuid.NewV4().String()
		collection.Update(bson.M{"email": user.Email}, bson.M{"$set": bson.M{"api_key": user.APIKey}})
		c.JSON(http.StatusCreated, gin.H{
			"api_key": user.APIKey,
			"email":   user.Email,
			"name":    user.Name,
		})
	}
}

// Logout returns the gin handling function for logout requests
func Logout(s *mgo.Session) gin.HandlerFunc {
	return func(c *gin.Context) {
		session := s.Copy()
		defer session.Close()

		collection := session.DB(global.DBName).C(global.CollectionUsers)
		if collection.Update(bson.M{"api_key": c.MustGet("api-key").(string)}, bson.M{"$set": bson.M{"api_key": nil}}) != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
			return
		}

		c.JSON(http.StatusOK, gin.H{"message": "Logged out"})
	}
}

// Register returns the gin handling function for registration requests
func Register(s *mgo.Session) gin.HandlerFunc {
	return func(c *gin.Context) {
		session := s.Copy()
		defer session.Close()

		var user structs.User
		decoder := json.NewDecoder(c.Request.Body)

		if decoder.Decode(&user) != nil || !nameRegex.MatchString(user.Name) || !emailRegex.MatchString(user.Email) {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid body"})
			return
		}

		now := time.Now()

		user.Password = sha256(user.Password)
		user.APIKey = uuid.NewV4().String()
		user.RegistrationMonth = structs.Month{now.Year(), int(now.Month()) - 1}
		user.Categories = []string{}

		collection := session.DB(global.DBName).C(global.CollectionUsers)

		if err := collection.Insert(user); err != nil {
			if mgo.IsDup(err) {
				c.JSON(http.StatusBadRequest, gin.H{"error": "E-Mail is already used"})
				return
			}

			c.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
			return
		}

		c.JSON(http.StatusCreated, gin.H{
			"api_key": user.APIKey,
			"email":   user.Email,
			"name":    user.Name,
		})
	}
}

// CheckAuth returns the gin handling function for check auth requests
func CheckAuth(s *mgo.Session) gin.HandlerFunc {
	return func(c *gin.Context) {
		session := s.Copy()
		defer session.Close()

		collection := session.DB(global.DBName).C(global.CollectionUsers)

		var user structs.User
		if collection.Find(bson.M{"api_key": c.MustGet("api-key").(string)}).One(&user) != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"auth":  true,
			"email": user.Email,
			"name":  user.Name,
		})
	}
}

// Update updates the stored user data in MongoDB
func Update(s *mgo.Session) gin.HandlerFunc {
	return func(c *gin.Context) {
		session := s.Copy()
		defer session.Close()

		collection := session.DB(global.DBName).C(global.CollectionUsers)

		var stored structs.User
		if collection.Find(bson.M{"api_key": c.MustGet("api-key").(string)}).One(&stored) != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
			return
		}

		var user structs.UserUpdate
		decoder := json.NewDecoder(c.Request.Body)

		if decoder.Decode(&user) != nil || !nameRegex.MatchString(user.Name) || !emailRegex.MatchString(user.Email) {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid body"})
			return
		}

		update := bson.M{"email": user.Email, "name": user.Name}

		if len(user.Password) > 0 && len(user.NewPassword) > 0 {
			if stored.Password == sha256(user.Password) {
				update["password"] = sha256(user.NewPassword)
			} else {
				c.JSON(http.StatusUnauthorized, gin.H{"error": "Wrong password"})
				return
			}
		}

		if collection.UpdateId(stored.ID, bson.M{"$set": update}) != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
			return
		}

		c.JSON(http.StatusOK, gin.H{"message": "Successfully updated"})
	}
}

// Delete deletes the user from MongoDB
func Delete(s *mgo.Session) gin.HandlerFunc {
	return func(c *gin.Context) {
		session := s.Copy()
		defer session.Close()

		collection := session.DB(global.DBName).C(global.CollectionUsers)
		if collection.Remove(bson.M{"api_key": c.MustGet("api-key").(string)}) != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
			return
		}

		c.JSON(http.StatusOK, gin.H{"message": "Successfully deleted"})
	}
}
