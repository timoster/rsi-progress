package endpoints

import (
	"encoding/json"
	"regexp"
	"strconv"
	"time"

	"backend/global"
	"backend/structs"

	"net/http"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var categoryRegex *regexp.Regexp

func init() {
	categoryRegex, _ = regexp.Compile("^[\\da-zA-Z\\s]+$")
}

func clamp(value int, min int, max int) int {
	if value > max {
		return max
	}

	if value < min {
		return min
	}

	return value
}

// GetCategories returns all user defined categories
func GetCategories(s *mgo.Session) gin.HandlerFunc {
	return func(c *gin.Context) {
		session := s.Copy()
		defer session.Close()

		var user structs.User
		collection := session.DB(global.DBName).C(global.CollectionUsers)
		if collection.Find(bson.M{"api_key": c.MustGet("api-key").(string)}).One(&user) != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
			return
		}

		c.JSON(http.StatusCreated, gin.H{"categories": user.Categories})
	}
}

// AddCategory adds a category to the user
func AddCategory(s *mgo.Session) gin.HandlerFunc {
	return func(c *gin.Context) {
		session := s.Copy()
		defer session.Close()

		var data interface{}
		decoder := json.NewDecoder(c.Request.Body)

		if decoder.Decode(&data) != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid body"})
			return
		}

		categoryName, valid := data.(map[string]interface{})["name"].(string)

		if !valid || !categoryRegex.MatchString(categoryName) {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid body"})
			return
		}

		var user structs.User
		collection := session.DB(global.DBName).C(global.CollectionUsers)
		if collection.Find(bson.M{"api_key": c.MustGet("api-key").(string)}).One(&user) != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
			return
		}

		for _, name := range user.Categories {
			if name == categoryName {
				c.JSON(http.StatusConflict, gin.H{"error": "Category already exsits"})
				return
			}
		}

		user.Categories = append(user.Categories, categoryName)

		if collection.Update(bson.M{"api_key": c.MustGet("api-key").(string)}, bson.M{"$set": bson.M{"categories": user.Categories}}) != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
			return
		}

		c.JSON(http.StatusCreated, gin.H{"categories": user.Categories})
	}
}

// GetRegistrationMonth returns the month of registration
func GetRegistrationMonth(s *mgo.Session) gin.HandlerFunc {
	return func(c *gin.Context) {
		session := s.Copy()
		defer session.Close()

		var user structs.User
		collection := session.DB(global.DBName).C(global.CollectionUsers)
		if collection.Find(bson.M{"api_key": c.MustGet("api-key").(string)}).One(&user) != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
			return
		}

		c.JSON(http.StatusCreated, gin.H{"month": user.RegistrationMonth})
	}
}

// GetReports returns all report data for the given month
func GetReports(s *mgo.Session) gin.HandlerFunc {
	return func(c *gin.Context) {
		session := s.Copy()
		defer session.Close()

		year, err1 := strconv.Atoi(c.Param("year"))
		month, err2 := strconv.Atoi(c.Param("month"))

		if err1 != nil || err2 != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid parameters"})
			return
		}

		var user structs.User
		collection := session.DB(global.DBName).C(global.CollectionUsers)
		if collection.Find(bson.M{"api_key": c.MustGet("api-key").(string)}).One(&user) != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
			return
		}

		reports := []structs.Report{}
		collection = session.DB(global.DBName).C(global.CollectionReports)
		if collection.Find(bson.M{"user": user.ID, "month": structs.Month{Year: year, Month: month}}).All(&reports) != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
			return
		}

		c.JSON(http.StatusCreated, gin.H{"reports": reports})
	}
}

// AddReport adds report to given month
func AddReport(s *mgo.Session) gin.HandlerFunc {
	return func(c *gin.Context) {
		session := s.Copy()
		defer session.Close()

		var report structs.Report
		decoder := json.NewDecoder(c.Request.Body)
		if decoder.Decode(&report) != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid body"})
			return
		}

		var user structs.User
		collection := session.DB(global.DBName).C(global.CollectionUsers)
		if collection.Find(bson.M{"api_key": c.MustGet("api-key").(string)}).One(&user) != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
			return
		}

		categories := []structs.Category{}

		for _, category := range report.Categories {
		nameCheck:
			for _, name := range user.Categories {
				if name == category.Name {
					category.Time = clamp(category.Time, 0, 10)
					category.Skill = clamp(category.Skill, 0, 10)
					category.Pain = clamp(category.Pain, 0, 10)
					categories = append(categories, category)
					break nameCheck
				}
			}
		}

		now := time.Now()
		month := structs.Month{Year: now.Year(), Month: int(now.Month()) - 1}
		day := now.Day() - 1

		collection = session.DB(global.DBName).C(global.CollectionReports)
		count, err := collection.Find(bson.M{"user": user.ID, "month": month, "day": day}).Count()

		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
			return
		}

		if count > 0 {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Already reported today"})
			return
		}

		insert := structs.Report{
			User:       user.ID,
			Month:      month,
			Day:        day,
			Categories: categories,
		}

		if collection.Insert(insert) != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
			return
		}

		c.JSON(http.StatusCreated, gin.H{"message": "Successfully created"})
	}
}

// AllowedToReport returns whether user can report or not
func AllowedToReport(s *mgo.Session) gin.HandlerFunc {
	return func(c *gin.Context) {
		session := s.Copy()
		defer session.Close()

		var user structs.User
		collection := session.DB(global.DBName).C(global.CollectionUsers)
		if collection.Find(bson.M{"api_key": c.MustGet("api-key").(string)}).One(&user) != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
			return
		}

		now := time.Now()
		month := structs.Month{Year: now.Year(), Month: int(now.Month()) - 1}
		day := now.Day() - 1

		collection = session.DB(global.DBName).C(global.CollectionReports)
		count, err := collection.Find(bson.M{"user": user.ID, "month": month, "day": day}).Count()

		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
			return
		}

		if count > 0 {
			c.JSON(http.StatusOK, gin.H{"allowed": false})
		} else {
			c.JSON(http.StatusOK, gin.H{"allowed": true})
		}
	}
}
