import { Component } from "@angular/core";
import { AuthService } from "./services/auth.service";
import { AuthMode } from "./structs/auth.struct";

class MenuItem {
    name: string;
    link: string;
    authMode?: AuthMode;
}

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.less"]
})
export class AppComponent {

    public authMode: AuthMode;

    public menuEntries: [MenuItem] = [
        { name: "Home", link: "/home" },

        // Not auth only
        { name: "Login", link: "/login", authMode: AuthMode.NOT_AUTH },
        { name: "Register", link: "/register", authMode: AuthMode.NOT_AUTH },

        // Auth only
        { name: "Progress", link: "/progress", authMode: AuthMode.AUTH },
        { name: "Today", link: "/today", authMode: AuthMode.AUTH },
        { name: "Account", link: "/account", authMode: AuthMode.AUTH }
    ];

    constructor(
        protected auth: AuthService
    ) { }
}
