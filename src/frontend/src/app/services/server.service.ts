import { HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";

import { User } from "../structs/auth.struct";
import { Month, Category, Report } from "../structs/user.struct";

import { AuthService } from "./auth.service";

import { environment } from "../../environments/environment";

@Injectable()
export class ApiKeyService {

    public apiKey = null;
}

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(
        private apiKey: ApiKeyService
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!req.headers.has("Content-Type")) {
            req = req.clone({ headers: req.headers.set("Content-Type", "application/json") });
        }

        if (this.apiKey.apiKey && this.apiKey.apiKey.length > 0) {
            req = req.clone({ headers: req.headers.set("X-Api-Key", this.apiKey.apiKey) });
        }

        req = req.clone({ headers: req.headers.set("Accept", "application/json") });
        return next.handle(req);
    }
}

@Injectable()
export class ServerService {

    constructor(
        private http: HttpClient
    ) { }

    public login(user: User): Observable<any> {
        return this.http.post(environment.apiUrl + "auth/login", JSON.stringify(user));
    }

    public register(user: User): Observable<any> {
        return this.http.post(environment.apiUrl + "auth/register", JSON.stringify(user));
    }

    public logout(): Observable<any> {
        return this.http.get(environment.apiUrl + "auth/logout");
    }

    public checkAuth(): Observable<any> {
        return this.http.get(environment.apiUrl + "auth/check");
    }

    public deleteAccount(): Observable<any> {
        return this.http.delete(environment.apiUrl + "auth/delete");
    }

    public updateAccount(user: User): Observable<any> {
        return this.http.post(environment.apiUrl + "auth/update", JSON.stringify(user));
    }

    public getCategories(): Observable<any> {
        return this.http.get(environment.apiUrl + "user/categories");
    }

    public addCategory(name: string): Observable<any> {
        return this.http.post(environment.apiUrl + "user/categories", `{"name":"${name}"}`);
    }

    public getRegistrationMonth(): Observable<any> {
        return this.http.get(environment.apiUrl + "user/registration");
    }

    public getReports(month: Month): Observable<any> {
        return this.http.get(environment.apiUrl + `user/reports/${month.year}/${month.month}`);
    }

    public addReport(report: Report): Observable<any> {
        return this.http.post(environment.apiUrl + "user/reports", JSON.stringify(report));
    }

    public allowedToReport(): Observable<any> {
        return this.http.get(environment.apiUrl + "user/allowed");
    }
}
