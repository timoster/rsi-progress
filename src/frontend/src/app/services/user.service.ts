import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Operator } from "rxjs/Operator";
import { Month, Category, Report } from "../structs/user.struct";

import { ServerService } from "../services/server.service";
import { ToastrService } from "ngx-toastr";

@Injectable()
export class UserService {

    constructor(
        private server: ServerService,
        private toastr: ToastrService
    ) { }

    public getRegistrationMonth(): Observable<Month> {
        return new Observable(observer => {
            this.server.getRegistrationMonth().subscribe(resp => {
                observer.next(resp["month"]);
            }, error => {
                observer.error(error.error);
            });
        });
    }

    public getCategories(): Observable<string[]> {
        return new Observable(observer => {
            this.server.getCategories().subscribe(resp => {
                observer.next(resp["categories"]);
            }, error => {
                observer.error(error.error);
            });
        });
    }

    public addCategory(name: string): Observable<string[]> {
        return new Observable(observer => {
            this.server.addCategory(name).subscribe(resp => {
                this.toastr.success("Added category");
                observer.next(resp["categories"]);
            }, error => {
                this.toastr.error(error.error.error);
                observer.error(error.error);
            });
        });
    }

    public getReports(month: Month): Observable<Report[]> {
        return new Observable(observer => {
            this.server.getReports(month).subscribe(resp => {
                const reports = [];
                resp["reports"].forEach(report => {
                    const cs = [];
                    report.categories.forEach(c => cs.push(new Category(c.name, c.time, c.skill, c.pain)));
                    reports.push(new Report(report.day, cs));
                });

                observer.next(reports);
            }, error => {
                observer.error(error.error);
            });
        });
    }

    public addReport(report: Report): Observable<any> {
        return new Observable(observer => {
            this.server.addReport(report).subscribe(resp => {
                this.toastr.success("Added report");
                observer.next();
            }, error => {
                this.toastr.error(error.error.error);
                observer.error(error.error);
            });
        });
    }

    public allowedToReport(): Observable<boolean> {
        return new Observable(observer => {
            this.server.allowedToReport().subscribe(resp => {
                observer.next(resp["allowed"] || false);
            }, error => {
                observer.next(false);
            });
        });
    }
}
