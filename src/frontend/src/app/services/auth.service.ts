import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

import { AuthError, AuthMode, User } from "../structs/auth.struct";
import { ApiKeyService, ServerService } from "../services/server.service";
import { ToastrService } from "ngx-toastr";

const API_KEY_STORE = "rsiProgressApiKey";

@Injectable()
export class AuthService {

    public authMode: AuthMode = AuthMode.NOT_AUTH;
    public user = new BehaviorSubject<User>(null);

    constructor(
        private router: Router,
        private server: ServerService,
        private apiKey: ApiKeyService,
        private toastr: ToastrService
    ) {
        this.apiKey.apiKey = localStorage.getItem(API_KEY_STORE);

        if (this.apiKey.apiKey != null) {
            this.checkAuth().subscribe(v => {
                if (v) {
                    this.authMode = AuthMode.AUTH;
                } else {
                    localStorage.removeItem(API_KEY_STORE);
                    this.router.navigate(["/login"]);
                }
            });
        }
    }

    public checkAuth(): Observable<any> {
        return new Observable(observer => {
            this.server.checkAuth().subscribe(resp => {
                if (resp["auth"] !== undefined) {
                    this.user.next(new User(resp.email, resp.name, ""));
                    observer.next(true);
                }
            }, error => {
                observer.next(false);
            });
        });
    }

    public login(user: User): Observable<AuthError> {
        return new Observable(observer => {
            this.server.login(user).subscribe(resp => {
                this.setApiKey(resp["api_key"]);
                this.router.navigate(["/progress"]);
                this.user.next(new User(resp.email, resp.name, ""));
                observer.next(AuthError.NONE);
            }, error => {
                this.toastr.error("Invalid credentials", "Login failed");
            });
        });
    }

    public logout() {
        this.server.logout().subscribe(resp => {
            this.resetApiKey();
            this.router.navigate(["/login"]);
        });
    }

    public register(user: User): Observable<AuthError> {
        return new Observable(observer => {
            this.server.register(user).subscribe(resp => {
                this.setApiKey(resp["api_key"]);
                this.router.navigate(["/progress"]);
                this.user.next(new User(resp.email, resp.name, ""));
                this.toastr.success("Successfully created account");
                observer.next(AuthError.NONE);
            }, error => {
                this.toastr.error("Invalid data", "Registration failed");
            });
        });
    }

    public deleteAccount(): Observable<any> {
        return new Observable(observer => {
            this.server.deleteAccount().subscribe(resp => {
                this.resetApiKey();
                this.router.navigate(["/home"]);
                this.toastr.success("Account deleted");
                observer.next();
            }, error => {
                this.toastr.error(error.error.error, "Deletion failed");
            });
        });
    }

    public updateData(user: User, newPassword: string): Observable<any> {
        user["new_password"] = newPassword;

        return new Observable(observer => {
            this.server.updateAccount(user).subscribe(resp => {
                this.toastr.success("Updated data");
                observer.next();
            }, error => {
                this.toastr.error(error.error.error, "Update failed");
            });
        });
    }

    public isAuthorized() {
        return this.authMode === AuthMode.AUTH;
    }

    private resetApiKey() {
        localStorage.removeItem(API_KEY_STORE);
        this.apiKey.apiKey = null;
        this.authMode = AuthMode.NOT_AUTH;
    }

    private setApiKey(key: string) {
        localStorage.setItem(API_KEY_STORE, key);
        this.apiKey.apiKey = key;
        this.authMode = AuthMode.AUTH;
    }
}
