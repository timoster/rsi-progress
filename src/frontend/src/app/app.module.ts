import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { ToastrModule } from "ngx-toastr";

import { AppComponent } from "./app.component";
import { HomeComponent } from "./components/home/home.component";
import { LoginComponent } from "./components/login/login.component";
import { ProgressComponent } from "./components/progress/progress.component";
import { TodayComponent } from "./components/today/today.component";
import { AccountComponent } from "./components/account/account.component";
import { PrivacyPolicyComponent } from "./components/privacy-policy/privacy-policy.component";

import { ProgressCalendarComponent } from "./components/progress-calendar/progress-calendar.component";
import { ProgressAnalysisComponent } from "./components/progress-analysis/progress-analysis.component";
import { MultiCheckboxComponent } from "./components/multi-checkbox/multi-checkbox.component";
import { VoteBarComponent } from "./components/vote-bar/vote-bar.component";

import { RepeatDirective } from "./directives/repeat.directive";

import { PadPipe } from "./pipes/pad.pipe";

import { ApiKeyService, ServerService, AuthInterceptor } from "./services/server.service";
import { AuthService } from "./services/auth.service";
import { UserService } from "./services/user.service";

const routes: Routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", component: HomeComponent },
    { path: "privacy", component: PrivacyPolicyComponent },

    // Not auth only
    { path: "login", component: LoginComponent, data: { login: true } },
    { path: "register", component: LoginComponent, data: { login: false } },

    // Auth Only
    { path: "progress", component: ProgressComponent },
    { path: "progress/:type", component: ProgressComponent },
    { path: "today", component: TodayComponent },
    { path: "account", component: AccountComponent }
];

@NgModule({
    declarations: [
        // Views
        AppComponent,
        HomeComponent,
        LoginComponent,
        ProgressComponent,
        TodayComponent,
        AccountComponent,
        PrivacyPolicyComponent,

        // Components
        ProgressCalendarComponent,
        ProgressAnalysisComponent,
        MultiCheckboxComponent,
        VoteBarComponent,

        // Directives
        RepeatDirective,

        // Pipes
        PadPipe
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        BrowserAnimationsModule,
        NgxChartsModule,
        RouterModule.forRoot(routes),
        ToastrModule.forRoot()
    ],
    providers: [
        ApiKeyService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        },
        ServerService,
        AuthService,
        UserService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
