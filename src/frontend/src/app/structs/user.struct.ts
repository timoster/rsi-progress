export class Month {
    constructor(
        public year: number,
        public month: number
    ) { }

    public getDays() {
        const date = new Date(this.year, this.month + 1, 0);
        return date.getDate();
    }
}

export class Category {
    constructor(
        public name: string,
        public time: number,
        public skill: number,
        public pain: number
    ) { }

    public day: number;

    public stress = 0;
    public progress = 0;

    public process() {
        this.calcStress();
        this.calcProgress();
    }

    private calcStress() {
        if (this.pain > 5) {
            this.stress = Math.ceil(this.time * 0.6);
        } else {
            this.stress = Math.ceil(this.time * 0.75 + this.pain * 0.25);
        }

        this.stress = 10 - Math.min(Math.max(this.stress, 0), 10);
    }

    private calcProgress() {
        if (this.stress > 5) {
            this.progress = Math.ceil(this.skill * 0.5);
        } else {
            this.progress = 4 + Math.ceil((this.skill * 0.75 + this.stress * 0.25) * 0.5);
        }

        this.progress = Math.min(Math.max(this.progress, 0), 10);
    }
}

export class Report {
    constructor(
        public day: number,
        public categories: Category[]
    ) { }

    public process() {
        this.categories.forEach(c => {
            c.day = this.day;
            c.process();
        });
    }
}
