export enum AuthMode {
    IGNORE,
    AUTH,
    NOT_AUTH
}

export enum AuthError {
    NONE,
    CONNECTION,
    INVALID_DATA
}

export class User {
    constructor(
        public email: string,
        public name: string,
        public password: string
    ) { }
}
