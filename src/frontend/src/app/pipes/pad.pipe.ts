import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "pad" })
export class PadPipe implements PipeTransform {

    transform(value: any, digits: number): string|null {
        let out: string = value.toString();

        while (out.length < digits) {
            out = "0" + out;
        }

        return out;
    }
}