import { Directive, Input, TemplateRef, ViewContainerRef } from "@angular/core";

export class RepeatContext {
    constructor(public index: number) { }
}

@Directive({ selector: "[modelRepeat]" })
export class RepeatDirective {

    constructor(
        private templateRef: TemplateRef<RepeatContext>,
        private viewContainer: ViewContainerRef
    ) { }

    @Input() set modelRepeat(times: number) {
        if (this.viewContainer.length !== times) {
            this.viewContainer.clear();

            for (let i = 0; i < times; i++) {
                this.viewContainer.createEmbeddedView(this.templateRef, new RepeatContext(i));
            }
        }
    }
}