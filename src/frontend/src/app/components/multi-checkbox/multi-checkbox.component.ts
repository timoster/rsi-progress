import { Component, HostListener, ElementRef, Input, Output, EventEmitter } from "@angular/core";

@Component({
    selector: "multi-checkbox",
    templateUrl: "./multi-checkbox.component.html",
    styleUrls: ["./multi-checkbox.component.less"]
})
export class MultiCheckboxComponent {

    @Input() public defaultText = "Select to filter";
    @Input() set options(options: [string]) {
        this.selectedItems.forEach((element, index, object) => {
            if (options.indexOf(element) === -1) {
                object.splice(index, 1);
            }
        });

        this.seletableItems = options;
        this.seletableItems.sort();
    }

    @Output() public selectionChanged = new EventEmitter();

    public expanded = false;
    public selectionText = this.defaultText;

    protected seletableItems = new Array<string>();
    protected selectedItems = new Array<string>();

    constructor(
        private element: ElementRef
    ) { }

    @HostListener("document:click", ["$event"])
    onDocumentClick(event: MouseEvent) {
        if (this.expanded && !this.element.nativeElement.contains(event.target)) {
            this.toggle();
        }
    }

    toggle() {
        this.expanded = !this.expanded;
    }

    onChange(target: any) {
        const i = this.selectedItems.indexOf(target.value);

        if (target.checked && i === -1) {
            this.selectedItems.push(target.value);
        } else if (!target.checked && i >= -1) {
            this.selectedItems.splice(i, 1);
        }

        this.selectedItems.sort();
        this.selectionChanged.emit(this.selectedItems);

        if (this.selectedItems.length > 0) {
            this.selectionText = this.selectedItems.reduce((prev, cur) => prev += ", " + cur);
        } else {
            this.selectionText = this.defaultText;
        }
    }
}