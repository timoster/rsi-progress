import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../services/auth.service";
import { User } from "../../structs/auth.struct";

@Component({
    selector: "app-account",
    templateUrl: "./account.component.html",
    styleUrls: ["./account.component.less"]
})
export class AccountComponent implements OnInit {

    public user: User = new User("", "", "");
    public oldPassword = "";
    public newPassword = "";

    public deleteSecurity = false;

    constructor(
        private auth: AuthService
    ) { }

    ngOnInit() {
        this.auth.user.subscribe(v => this.user = v);
    }

    save() {
        const newPassword = this.oldPassword.length + this.newPassword.length > 0;
        const data = new User(this.user.email, this.user.name, newPassword ? this.oldPassword : "");
        this.auth.updateData(data, newPassword ? this.newPassword : "").subscribe(v => {
        });
    }

    deleteAccount() {
        this.auth.deleteAccount().subscribe(v => {
        });
    }
}
