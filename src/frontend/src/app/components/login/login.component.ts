import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { AuthService } from "../../services/auth.service";
import { AuthError, User } from "../../structs/auth.struct";

enum FormState {
    LOGIN,
    REGISTER
}

@Component({
    selector: "app-login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.less"]
})
export class LoginComponent implements OnInit {

    public user: User = new User("", "", "");
    public error: AuthError = AuthError.NONE;

    public FormState = FormState;
    public state: FormState;

    constructor(
        private auth: AuthService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.state = this.route.snapshot.data["login"] ? FormState.LOGIN : FormState.REGISTER;
    }

    login() {
        this.auth.login(this.user).subscribe(v => this.error = v);
    }

    register() {
        this.auth.register(this.user).subscribe(v => this.error = v);
    }

    isError() {
        return this.error !== AuthError.NONE;
    }
}
