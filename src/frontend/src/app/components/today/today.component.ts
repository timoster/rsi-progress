import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserService } from "../../services/user.service";
import { Report, Category } from "../../structs/user.struct";

@Component({
    selector: "app-today",
    templateUrl: "./today.component.html",
    styleUrls: ["./today.component.less"]
})
export class TodayComponent implements OnInit {

    public canInsert = true;
    public isAdding = false;
    public isSelecting = true;
    public newCategoryName = "";

    public selections: string[] = [];
    public categories: Category[] = [];

    constructor(
        private router: Router,
        private user: UserService
    ) { }

    ngOnInit() {
        this.user.getCategories().subscribe(v => v.forEach(c => this.categories.push(new Category(c, 0, 0, 0))));
        this.user.allowedToReport().subscribe(v => this.canInsert = v);
    }

    addCategory() {
        const name = this.newCategoryName.trim();

        if (name.length > 0) {
            this.user.addCategory(name).subscribe(v => {
                this.categories = [];
                v.forEach(c => this.categories.push(new Category(c, 0, 0, 0)));
            });
        }

        this.isAdding = false;
        this.newCategoryName = "";
    }

    selectionChange(target) {
        const index = this.selections.indexOf(target.value);

        if (target.checked && index === -1) {
            this.selections.push(target.value);
        }

        if (!target.checked && index > -1) {
            this.selections.splice(index, 1);
        }

        this.selections.sort();
    }

    back() {
        this.isSelecting = true;
    }

    next() {
        if (!this.isSelecting) {
            const reportData = [];
            this.categories.forEach(category => {
                if (this.selections.indexOf(category.name) > -1) {
                    reportData.push(category);
                }
            });

            this.user.addReport(new Report(null, reportData)).subscribe(v => {
                this.router.navigate(["/progress"]);
            });

            return;
        }

        if (this.selections.length > 0) {
            this.isSelecting = false;
        }
    }
}
