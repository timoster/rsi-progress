import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
    selector: "vote-bar",
    templateUrl: "./vote-bar.component.html",
    styleUrls: ["./vote-bar.component.less"]
})
export class VoteBarComponent {

    @Input() public desc: string;
    @Output() public value = new EventEmitter<number>();

    private currentValue = 0;
    public tempValue = -1;

    setValue(value: number) {
        this.currentValue = value;
        this.value.next(value);
    }

    getValue() {
        return this.tempValue > -1 ? this.tempValue : this.currentValue;
    }
}