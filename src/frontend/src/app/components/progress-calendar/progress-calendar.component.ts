import { Component, Input } from "@angular/core";
import { Month, Category, Report } from "../../structs/user.struct";

const WEEK_DAYS = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

@Component({
    selector: "app-progress-calendar",
    templateUrl: "./progress-calendar.component.html",
    styleUrls: ["./progress-calendar.component.less"]
})
export class ProgressCalendarComponent {

    @Input() public data = new Map<number, Report>();
    @Input() public filters = new Array<string>();
    @Input() set month(month: Month) {
        this.currentMonth = month;
        this.date = new Date(month.year, month.month, 1);
    }

    private currentMonth: Month;
    public date: Date = new Date();

    dayName(day: number) {
        return WEEK_DAYS[(this.date.getDay() + day) % 7];
    }

    getDaysInMonth() {
        const days = [];

        for (let i = 0; i < this.currentMonth.getDays(); i++) {
            days.push(i);
        }

        return days;
    }
}
