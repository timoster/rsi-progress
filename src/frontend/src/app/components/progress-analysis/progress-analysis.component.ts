import { Component, Input } from "@angular/core";
import { Month, Category, Report } from "../../structs/user.struct";
import * as shape from "d3-shape";

class Graph {
    constructor(
        public label: string,
        public data,
        public curve = shape.curveBasis
    ) { }
}

const TYPES = ["Duration", "Skill", "Pain", "Progress"];
const ACCESSORS = [
    (category: Category) => category.time,
    (category: Category) => category.skill,
    (category: Category) => category.pain,
    (category: Category) => category.progress
];

@Component({
    selector: "app-progress-analysis",
    templateUrl: "./progress-analysis.component.html",
    styleUrls: ["./progress-analysis.component.less"]
})
export class ProgressAnalysisComponent {

    @Input() public month: Month;
    @Input() set filters(filters: Array<string>) {
        this._filters = filters;
        this.updateGraphs();
    }
    @Input() set data(data: Map<number, Report>) {
        this._data = data;
        this.updateGraphs();
    }

    private _filters = new Array<string>();
    private _data = new Map<number, Report>();

    public graphs = new Array<Graph>();

    public avgPauseLength = 0;
    public avgPauseBetween = 0;

    private updateGraphs() {
        this.graphs = [];

        const days = this.month.getDays();
        const categories = new Map<string, Array<Category>>();
        const pauseData = new Array<number>(days).fill(0);

        this._data.forEach((report: Report, day: number) => {
            report.categories.forEach(category => {
                if (this._filters.length === 0 || this._filters.indexOf(category.name) > -1) {
                    if (!categories.has(category.name)) {
                        categories.set(category.name, new Array<Category>());
                    }

                    categories.get(category.name).push(category);
                }
            });
        });

        for (let i = 0; i < TYPES.length; i++) {
            const aggregation = new Array<{ times: number, value: number }>(days);
            const streakData = [];

            for (let i2 = 0; i2 < aggregation.length; i2++) {
                aggregation[i2] = { times: 0, value: 0 };
            }

            categories.forEach((list: Array<Category>, name: string) => {
                list.sort((a: Category, b: Category) => a.day - b.day).forEach(category => {
                    aggregation[category.day].times++;
                    aggregation[category.day].value += ACCESSORS[i](category);
                    pauseData[category.day]++;
                });
            });

            for (let i2 = 0; i2 < aggregation.length; i2++) {
                if (aggregation[i2].times > 0) {
                    streakData.push({ "name": (i2 + 1).toString(), "value": (aggregation[i2].value / aggregation[i2].times) });
                }
            }

            this.graphs.push(new Graph(TYPES[i], [{ "name": "AvgTrend", "series": streakData }]));
        }

        this.calcTrends();

        const pauseGraph = [];
        const pauseLengths = [];
        const daysBetween = [];

        let lastState = false;
        let counter = 0;

        for (let i = 0; i < pauseData.length; i++) {
            const isPause = pauseData[i] === 0;
            pauseGraph.push({ "name": (i + 1).toString(), "value": isPause ? 1 : 0 });

            if (isPause !== lastState) {
                if (lastState) {
                    pauseLengths.push(counter);
                } else {
                    daysBetween.push(counter);
                }

                lastState = isPause;
                counter = 0;
            }

            counter++;
        }

        this.avgPauseLength = pauseLengths.length > 0 ? +(pauseLengths.reduce((a, b) => a + b) / pauseLengths.length).toFixed(2) : 0;
        this.avgPauseBetween = daysBetween.length > 0 ? +(daysBetween.reduce((a, b) => a + b) / daysBetween.length).toFixed(2) : 0;
        this.graphs.push(new Graph("Pauses", [{"name" : "Pauses", "series": pauseGraph }], shape.curveStep));
    }

    private calcTrends() {
        this.graphs.forEach(graph => {
            if (graph.data[0].series.length === 0) {
                return;
            }

            let minimum = 10;
            let maximum = 0;
            let last = 0;

            graph.data[0].series.forEach(data => {
                if (data.value > maximum) {
                    maximum = data.value;
                }

                if (data.value < minimum) {
                    minimum = data.value;
                }

                last = data.value;
            });

            const middle = minimum + (maximum - minimum) / 2.0;
            const diff = (last - middle) / 3.0;

            const newData = [
                { "name": graph.data[0].series[0].name, "value": middle - diff },
                { "name": graph.data[0].series[graph.data[0].series.length - 1].name, "value": middle + diff }
            ];

            graph.data.push({ "name": "Trend", "series": newData });
        });
    }
}
