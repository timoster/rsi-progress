import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Month, Report } from "../../structs/user.struct";

import { UserService } from "../../services/user.service";

const MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

@Component({
    selector: "app-progress",
    templateUrl: "./progress.component.html",
    styleUrls: ["./progress.component.less"]
})
export class ProgressComponent implements OnInit {

    public currentMonth: Month;
    public startMonth: Month;

    public filters = new Array<string>();
    public categories = new Array<string>();
    public years = new Array<number>();
    public months = new Array<number>();
    public data: Map<number, Report> = new Map<number, Report>();

    public isCalendar = true;

    constructor(
        private user: UserService,
        private router: Router,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        const date = new Date();
        this.currentMonth = new Month(date.getFullYear(), date.getMonth());
        this.user.getCategories().subscribe(v => this.categories = v);

        this.user.getRegistrationMonth().subscribe(month => {
            this.startMonth = month;

            for (let i = 0; i <= date.getFullYear() - month.year; i++) {
                this.years.push(month.year + i);
            }

            this.updateYear(date.getFullYear());
        });

        const type = this.route.snapshot.paramMap.get("type");
        if (type === "analysis") {
            this.showAnalysis();
        } else {
            this.showCalendar();
        }
    }

    showCalendar() {
        this.isCalendar = true;
        this.router.navigate(["/progress/calendar"]);
    }

    showAnalysis() {
        this.isCalendar = false;
        this.router.navigate(["/progress/analysis"]);
    }

    filterUpdate(filters: [string]) {
        this.filters = new Array<string>();
        filters.forEach(v => this.filters.push(v));
    }

    updateYear(year: any) {
        this.currentMonth = new Month(+year, +year === this.startMonth.year ? this.startMonth.month : this.currentMonth.month);
        this.fetchData();

        this.months = [];

        for (let i = (+year === this.startMonth.year ? this.startMonth.month : 0); i < 12; i++) {
            this.months.push(i);
        }
    }

    updateMonth(month: any) {
        this.currentMonth = new Month(this.currentMonth.year, +month);
        this.fetchData();
    }

    private fetchData() {
        this.data = new Map<number, Report>();
        this.user.getReports(this.currentMonth).subscribe(v => v.forEach(o => {
            o.process();
            this.data.set(o.day, o);
        }));
    }

    getMonthName(month: number) {
        return MONTHS[month];
    }
}
