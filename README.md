# rsi-progress
An activity tracker to help with the recovery of RSI symptoms

### Deployment
1. Create config file using `./create_conf.sh`
2. Replace `192.168.2.200` in `src/frontend/Dockerfile` with the IP of your host system
3. `./deploy.sh --db --proxy --backend --frontend`
